"use strict"

// 1
let arrString = ["travel", "hello", "eat", "ski", "lift"];
let countString = function(someArr){
    let totalString = null;
    let newArrStrings = []
    someArr.forEach(el=>{
        if(el.length > 3){
            totalString += 1;
            newArrStrings.push(el)
        }
    })
    return `total- ${totalString}: ${newArrStrings}`
}
console.log(countString(arrString));

// 2
let listUsers = [
    {name: "Вінсент", age: 15, sex: "чоловіча"},
    {name: "Олег", age: 35, sex: "чоловіча"},
    {name: "Іра", age: 25, sex: "жіноча"},
    {name: "Марк", age: 45, sex: "чоловіча"}
]
let listUsersFilter = listUsers.filter((el)=>{
    return el.sex === "чоловіча"
})
console.log(listUsersFilter);

// 3

function filterBy(arr, type){
    let arrBack = [];
    arr.forEach((el)=>{
        if(typeof el !== type){
            arrBack.push(el)
        }
    })
    return arrBack
}


let testArr= ['hello', 'world', 23, '23', null]

console.log(filterBy(testArr, 'string'))
